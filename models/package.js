const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PackageSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    hotel: {
        type: String,
        required: true
    },
    website: {
        type: String,
        required: true
    },
    price: {
        type: Number,
        required: true
    },
    rooms: {
        type: Number,
        required: true
    },
    expiry: {
        type: Date,
        required: true
    }
}, { timeStamp: true});

const Package = mongoose.model('Package', PackageSchema);
module.exports = Package;