const Package = require('../models/package');

const package_index = (req,res) => {
    const pageNo = parseInt(req.body.currentPage);
    const pageSize = parseInt(req.body.pageSize);
    const totalPages = parseInt(req.body.totalPages);
    let query = {};
    query.skip = pageSize * ( pageNo - 1);
    query.limit = pageSize;
    let list = {};
    if(totalPages === 0){
    Package.countDocuments().then(count => {
        list.totalPages = Math.ceil(count/pageSize);
    }).catch(err => {
        console.log(err);
    })
    }else{
        list.totalPages = totalPages;
    }
    
    const packages = Package.find({},{},query).sort({price: -1})
        .then(result => {
            list.packages = result;
            res.json(list);
        })
        .catch(err => console.log(err));


}

const package_create = (req, res) => {
    let newPackage = {...req.body, expiry: new Date(req.body.expiry)};
    const package = new Package(newPackage);
    package.save()
        .then(result => {
            res.json('Done !')
        }).catch(err => {
            console.log(err);
        })
}

const package_details = (req, res) => {
    const id = req.params.id;
    Package.findById(id)
        .then(result => {
            res.json(result);
        }).catch(err => {
            console.log(err);
        })
}

module.exports = {
    package_index,
    package_create,
    package_details
}