const port = 5000;
const express = require('express');
const cors = require('cors');
const app = express();
const mongoose = require('mongoose');
const packageRoutes = require('./routes/packageRoutes');

const dbURI = 'mongodb+srv://mohak:mohak123@cluster0.mmeap.mongodb.net/hotel-packages?retryWrites=true&w=majority';
mongoose.connect(dbURI, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(result => {
    console.log('Server listening on port '+port);
    app.listen(port);
  })
  .catch(err => console.log(err));

app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use('/packages',packageRoutes);


