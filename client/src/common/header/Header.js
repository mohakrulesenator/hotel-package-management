import React from 'react';
import { Link, withRouter } from 'react-router-dom';
const Header = (props) => {
    return (
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <div className="container-fluid">
                <Link to = '/' className="navbar-brand" href="#">Hotel Package Management</Link>
                <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse d-flex flex-row-reverse" id="navbarNav">
                    <ul className="navbar-nav">
                        <li className="nav-item">
                            <Link to='/' className="nav-link" aria-current="page">Home</Link>
                        </li>
                        <li className="nav-item">
                            <Link to='/' className="nav-link">Pricing</Link>
                        </li>
                        <li className="nav-item">
                            <Link to='/' className="nav-link">Support</Link>
                        </li>
                        {props.history.location.pathname === '/new' ? null : <li className="nav-item">
                        <Link to = '/new' className="nav-link btn btn-primary btn-sm text-light">Add new package</Link>
                        </li>}
                    </ul>
                </div>
            </div>
        </nav>
    )
}

export default withRouter(Header);