import React from 'react';

const Footer = () => {
    return (
            <footer className="bg-light text-center text-lg-start fixed-bottom">
            <div className="text-center p-3 bg-light">
                © 2021 Copyright:&nbsp;&nbsp;
                <a className="text-dark" href="https://www.metricscurve.com/">Metrics Curve Technologies</a>
            </div>
            </footer>
    )
}

export default Footer;