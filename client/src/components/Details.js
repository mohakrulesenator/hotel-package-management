import React, { Component } from 'react';
import PackageService from '../service/PackageService';
import { Link } from 'react-router-dom';
import { format,toDate } from 'date-fns';

class Details extends Component {
    constructor(props) {
        super(props);
        this.state = {
            packageDetails: {},
            isLoading: false
        }
    }
    componentDidMount() {
        const { id } =  this.props.match.params;
        this.refresh(id);
    }

    refresh = (id) => {
        this.setState({isLoading: true});
        PackageService.getPackageDetails(id).then(response => {
            this.setState({packageDetails: response.data, isLoading: false}, () => console.log(response))
        }).catch(err =>{
        this.setState({isLoading: false});
         console.log(err)})
    }
    
    render() {
        const { packageDetails, isLoading } = this.state;
        return (
            <div className="text-center w-75 my-4 mx-auto">
                <h1 className="display-4">PACKAGE DETAILS</h1>
                {!isLoading ? <div className="card">
                        <div className="card-header text-uppercase">
                            {packageDetails.name}
                        </div>
                        <div className="card-body">
                            <h5 className="card-title h2">{'Rs. ' + packageDetails.price + ' / month'}</h5>
                            <p className="card-text">{packageDetails.rooms + ' Rooms'}</p>
                            <p className="card-text text-capitalize" >{packageDetails.hotel}</p>
                            <p className = "card-text">{packageDetails.website}</p>
                            <p className="card-text"><b>Validity</b> {new Date(packageDetails.expiry).toDateString()}</p>
                            <Link to='/' className="btn btn-info mr-4">Go back</Link>
                            <a href='#' className="btn btn-primary ml-4">Go to website</a>
                        </div>
                    </div> : <div className="spinner-border mx-auto" role="status">
                    <span className="visually-hidden"></span>
                </div>}
            </div>
        )
    }
}

export default Details;