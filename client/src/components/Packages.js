import React, { Component } from 'react';
import PackageService from '../service/PackageService';
import { format } from 'date-fns';
import { Link } from 'react-router-dom';

const helper = (count) => {
    const ar = [];
    for (let i = 0; i < count; i++) {
        ar.push(i);
    }
    return ar;
}

class Packages extends Component {
    constructor() {
        super();
        this.state = {
            packages: [],
            pageSize: 3,
            currentPage: 1,
            totalPages: 0,
            isLoading: false
        }
    }
    componentDidMount() {
        this.refresh();
    }

    refresh = () => {
        this.setState({ isLoading: true });
        PackageService.getPackages(this.state)
            .then(response => {
                const { packages, totalPages } = response.data;
                this.setState({ packages, totalPages, isLoading: false }, () => console.log(this.state));
            })
            .catch(err => {
                this.setState({ isLoading: false });
                console.log(err);
            });
    }

    handlePage = (currentPage) => {
        this.setState({ currentPage }, () => this.refresh());
    }

    render() {
        const { packages, pageSize, currentPage, totalPages, isLoading } = this.state;
        const list = packages.length > 0 ? packages.map(pack => {
            return (
                <div className="col-sm-4" key={pack._id}>
                    <div className="card">
                        <div className="card-header text-uppercase">
                            {pack.name}
                        </div>
                        <div className="card-body">
                            <h5 className="card-title h2">{'Rs. ' + pack.price + ' / month'}</h5>
                            {/* <p className="card-text">{pack.rooms + ' Rooms'}</p> */}
                            <p><a className="card-text text-capitalize" href={pack.website}>{pack.hotel}</a></p>
                            {/* <p className="card-text">{'Valid till ' + format(new Date(pack.expiry), 'dd/MM/yyyy')}</p> */}
                            <Link to={'/details/' + pack._id} className="btn btn-primary d-block">More Details</Link>
                        </div>
                    </div>
                </div>
            )
        }) : (
                <p className="mx-auto text-uppercase h4">No packages available.</p>
            );
        return (
            <div className="text-center w-75 my-4 mx-auto">
                <h1 className="display-1">Hotel Packages</h1>
                <p className="lead">Lorem ipsum dolor sit amet consectetur adipisicing elit. Iure eius consequuntur magni, optio necessitatibus deleniti tenetur atque ad dolorem alias. Aspernatur odio eveniet animi, accusantium ratione nisi aliquid expedita consequuntur.</p>
                {isLoading ?
                    <div className="spinner-border mx-auto" role="status">
                        <span className="visually-hidden"></span>
                    </div> :
                    <div>
                        <div className="row my-4">
                            {list}
                        </div>
                        <nav aria-label="Page navigation">
                            <ul className="pagination justify-content-end">
                                {helper(totalPages).map(p =>
                                    <li className="page-item" key={p}><button className="page-link" onClick={() => this.handlePage(p + 1)}> {p + 1} </button></li>
                                )}
                            </ul>
                        </nav>
                    </div>
                }

            </div>
        )
    }
}

export default Packages;