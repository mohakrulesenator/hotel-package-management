import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PackageService from '../service/PackageService';
import { isAfter } from 'date-fns';

class NewPackage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            pack: {
                name: '',
                rooms: '',
                hotel: '',
                website: '',
                price: '',
                expiry: ''
            },
            dateError: undefined,
            success: false,
            isLoading: false
        }
    }

    handleChange = (e) => {
        let { pack } = this.state;
        const key = e.target.name;
        let value = e.target.value;
        pack[key] = value;
        this.setState({ pack });
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.setState({isLoading: true});
        const today = new Date().getTime();
        let expiry = new Date(this.state.pack.expiry).getTime();
        // console.log(today,expiry, today > expiry);
        if (today > expiry) {
            this.setState({ dateError: 'Please enter valid expiry date', isLoading: false });
            return;
        }
        this.setState({ dateError: undefined });
        let { pack } = this.state;
        pack.expiry = expiry;
        this.setState({ pack });
        PackageService.addPackage(this.state.pack).then(response => {
            console.log(response);
            this.setState({isLoading: false, success: true});
            setTimeout(() => {
                this.props.history.push('/')
            }, 2000);
        }).catch(err =>{
            console.log(err)
            this.setState({isLoading: false, success: false});
        });
    }


    render() {
        let { pack, dateError, success, isLoading } = this.state;
        return (
            <div className="text-center w-75 my-4 mx-auto">
                <nav aria-label="breadcrumb">
                    <ol className="breadcrumb">
                        <li className="breadcrumb-item"><Link to="/">Home</Link></li>
                        <li className="breadcrumb-item active" aria-current="page">New Package</li>
                    </ol>
                </nav>
                {success ? <div className="alert alert-success" role="alert">
                            Package added successfully !
                </div> : null
                }
                {isLoading ? <div className="spinner-border mx-auto" role="status">
                    <span className="visually-hidden"></span>
                </div> : null}
                <form className="w-50 text-left mx-auto" onSubmit={this.handleSubmit}>
                    <div className="mb-2">
                        <label htmlFor="name" className="form-label">Package Name</label>
                        <input type="text" value={pack.name} className="form-control" id="name" name="name" onChange={this.handleChange} placeholder="Enter package name" required />
                    </div>
                    <div className="mb-2">
                        <label htmlFor="hotel" className="form-label">Hotel Name</label>
                        <input type="text" value={pack.hotel} className="form-control" id="hotel" name="hotel" onChange={this.handleChange} placeholder="Enter hotel name" required />
                    </div>
                    <div className="mb-2">
                        <label htmlFor="website" className="form-label">Hotel Website</label>
                        <input type="text" value={pack.website} className="form-control" id="website" onChange={this.handleChange} name="website" placeholder="Enter hotel website" required />
                    </div>
                    <div className="mb-2">
                        <label htmlFor="rooms" className="form-label">No. of rooms</label>
                        <input type="number" value={pack.rooms} className="form-control" id="rooms" name="rooms" min="1" onChange={this.handleChange} placeholder="Enter no. of rooms" required />
                    </div>
                    <div className="mb-2">
                        <label htmlFor="price" className="form-label">Package price</label>
                        <input type="number" value={pack.price} className="form-control" id="price" name="price" min="1" onChange={this.handleChange} placeholder="Enter price in Rs." required />
                    </div>
                    <div className="mb-2">
                        <label htmlFor="expiry" className="form-label">Package expiry date</label>
                        <input type="date" value={pack.expiry} className="form-control" id="expiry" onChange={this.handleChange} name="expiry" required />
                        {dateError ? <p className="small text-danger">{dateError}</p> : null}
                    </div>
                    <div className="text-center">
                        <button type="submit" className="btn btn-primary" disabled={isLoading}>Add Package</button>
                    </div>
                </form>


            </div>
        )
    }
}

export default NewPackage;