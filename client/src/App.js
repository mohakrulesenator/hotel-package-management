import React, { Component } from 'react';
import './styles.css';
import Packages from './components/Packages';
import NewPackage from './components/NewPackage';
import Details from './components/Details';
import Header from './common/header/Header';
import Footer from './common/footer/Footer';
import { BrowserRouter, Route} from 'react-router-dom';

class App extends Component {
  render() {
    return (
      <div>
      <BrowserRouter>
      <Header props = {this.props} />
        <div className="App">
          <Route exact path='/' component={Packages} />
          <Route path='/new' component={NewPackage} />
          <Route path='/details/:id' component={Details} />
        </div>
      </BrowserRouter>
      <Footer />
      </div>
    );
  }
}

export default App;
