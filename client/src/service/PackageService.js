import axios from 'axios';

class PackageService {
    static instance() {
        return new PackageService();
    }

    getPackages(req){
        return axios.post('/packages/list',req);
    }
    addPackage(req){
        return axios.post('/packages',req);
    }
    getPackageDetails(id){
        return axios.get('/packages/details/'+id);
    }
}
export default PackageService.instance();