const express = require('express');
const packageController = require('../controller/packageController');

const router = express.Router();

router.post('/list', packageController.package_index);
router.post('/', packageController.package_create);
router.get('/details/:id', packageController.package_details);

module.exports = router;
    
